FROM debian:stable
LABEL maintainer="dmpop@linux.com"
LABEL version="0.1"
LABEL description="Lilio Docker image"
RUN apt-get update
RUN apt-get install git mplayer php-cli alsa-utils -y
COPY . /usr/src/lilio
WORKDIR /usr/src/lilio
EXPOSE 8000
CMD [ "php", "-S", "0.0.0.0:8000" ]