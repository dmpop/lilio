<!DOCTYPE html>
<html lang="en">

<!-- Author: Dmitri Popov, dmpop@linux.com
	 License: GPLv3 https://www.gnu.org/licenses/gpl-3.0.txt -->

<head>
	<title>Lilio</title>
	<meta charset="utf-8">
	<link rel="shortcut icon" href="favicon.png" />
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/uikit@3.5.7/dist/css/uikit.min.css" />
	<script src="https://cdn.jsdelivr.net/npm/uikit@3.5.7/dist/js/uikit.min.js"></script>
	<script src="https://cdn.jsdelivr.net/npm/uikit@3.5.7/dist/js/uikit-icons.min.js"></script>
</head>

<body>
	<div class="uk-container uk-margin-small-top">
		<div class="uk-card uk-card-primary uk-card-body">
			<h1 class="uk-heading-line uk-text-center"><span>L i l i o</span></h1>
			<div class="uk-flex uk-flex-center">
				<p><?php $ip = shell_exec('sudo hostname -I');
					echo $ip; ?></p>
			</div>
			<?php
			$dir = "pls";
			$soundout = "Headphone";
			if (!file_exists($dir)) {
				mkdir($dir, 0777, true);
			}
			?>
			<form action=" " method="POST">
				<select class="uk-select" name="station">
				<option>Select station</option>
					<?php
					$files = glob($dir . "/*");
					foreach ($files as $file) {
						$filename = basename($file);
						$name = pathinfo($file)['extension'];
						echo "<option value='$filename'>" . pathinfo($file)['filename'] . "</option>";
					}
					?>
				</select>
				<button class="uk-button uk-button-primary uk-margin-top" type='submit' role='button' name='play'><span uk-icon="play"></span></button>
				<button class="uk-button uk-button-default uk-margin-top" type='submit' role='button' name='stop'><span uk-icon="ban"></span></button>
				<button class="uk-button uk-button-default uk-margin-top" type="button" uk-toggle="target: #modal">Help</button>
				<hr>
				<input class="uk-input" type="text" name="volume" placeholder="91">
				<button class="uk-button uk-button-primary uk-margin-top" type='submit' role='button' name='save'>Volume</button>
				<hr>
				<button class="uk-button uk-button-danger uk-margin-small-top" name="delete">Delete</button>
				<button class="uk-button uk-button-default uk-margin-small-top" name="shutdown">Shut down</button>
				<div id="modal" uk-modal>
					<div class="uk-modal-dialog uk-modal-body">
						<h2 class="uk-modal-title">Help</h2>
						<ul>
							<li>
								Use the <strong>Upload</strong> form to upload playlist files
							</li>
							<li>
								Choose the desired station, press <strong>Play</strong>
							</li>
							<li>
								Press <strong>Stop</strong> to stop streaming
							</li>
							<li>
								Press <strong>Delete</strong> to delete the currently selected playlist file
							</li>
						</ul>
						<p class="uk-text-right">
							<button class="uk-button uk-button-primary uk-modal-close" type="button">Close</button>
						</p>
					</div>
				</div>
			</form>
		</div>
		<div class="uk-container uk-margin-small-top">
			<div class="uk-card uk-card-default uk-card-body">
				<?php
				if (isset($_POST['upload'])) {
					$countfiles = count($_FILES['file']['name']);
					// looping all files
					for ($i = 0; $i < $countfiles; $i++) {
						$filename = $_FILES['file']['name'][$i];
						if (!file_exists($dir)) {
							mkdir($dir, 0777, true);
						}
						move_uploaded_file($_FILES['file']['tmp_name'][$i], $dir . DIRECTORY_SEPARATOR . $filename);
						echo "<script>";
						echo "window.location.href='.';";
						echo "</script>";
					}
				}
				?>
				<h2 class="uk-heading-line uk-text-center"><span>Upload</span></h2>
				<form method='post' action='' enctype='multipart/form-data'>
					<input class="uk-input" type="file" name="file[]" id="file" multiple>
					<button class="uk-button uk-button-primary uk-margin-top" type='submit' role='button' name='upload'>Upload</button>
				</form>
				<p class="uk-text-center">This is <a href="https://gitlab.com/dmpop/lilio">Lilio</a></p>
			</div>
			<?php
			if (isset($_POST['play'])) {
				echo "<script>";
				echo "UIkit.notification({message: 'Now streaming: " . ($_POST['station']) . " '});";
				echo "</script>";
				shell_exec('sudo killall mplayer > /dev/null 2>&1 & echo $!');
				shell_exec('sudo mplayer -playlist ' . $dir . '/' . escapeshellarg($_POST['station']) . ' > /dev/null 2>&1 & echo $!');
			}
			if (isset($_POST['delete'])) {
				unlink(escapeshellcmd($dir . '/' . $_POST['station']));
				echo "<script>";
				echo "window.location.href='.';";
				echo "</script>";
			}
			if (isset($_POST['stop'])) {
				shell_exec('sudo killall mplayer > /dev/null 2>&1 & echo $!');
			}
			if (isset($_POST['volume'])) {
				shell_exec('sudo amixer sset "' . $soundout . '" ' . ($_POST['volume']) . '% > /dev/null 2>&1 & echo $!');
			}
			if (isset($_POST['shutdown'])) {
				echo "<script>";
				echo "UIkit.notification({message: 'Shutdown completed'});";
				echo "</script>";
				shell_exec('sudo shutdown -h now > /dev/null 2>&1 & echo $!');
			}
			?>
		</div>
	</div>
</body>

</html>